
const formId = document.getElementById('formDiv');
const List = document.getElementById('showItems');


//ADD AN ITEM
formId.addEventListener('submit',(e)=>{
    if( document.getElementById('inputText').value != ''){
        e.preventDefault();        
        const newItem = document.getElementById('inputText').value;

        // list
        const li = document.createElement('li');
        li.className="listItem" 

        // checkbox
        const check = document.createElement('input')
        check.className = "form-check-input"
        check.type="checkbox"
        check.value=''
        // label
        const label = document.createElement('label');
        label.className ="form-check-label" ;
        label.appendChild(document.createTextNode(newItem));
        //delete button
        const newBtn = document.createElement('button');
        newBtn.className = "btn";
        newBtn.textContent="X";
        //appending
        li.appendChild(check);
        li.appendChild(label);
        li.appendChild(newBtn)
        List.appendChild(li);
        formId.reset()
    }
    const cardBody= document.getElementById('card-body')
    const hasItem = List.getElementsByTagName('li').length >= 1
    if(hasItem)
        cardBody.style.display="block"
})

//REMOVE AN ITEM
const ul = document.getElementById('showItems');
ul.addEventListener('click',(e)=>{
    const uList =e.target.parentElement.parentElement
    if(e.target.classList.contains('btn')){
            var li = e.target.parentElement;
            uList.removeChild(li)
    }
   if(uList.children.length==0){
    document.getElementById('card-body').style.display='none'
   }
   
})

//CHECKBOX
ul.addEventListener('change',(e)=>{
    if(e.target.classList.contains('form-check-input')){
        const targetNextSibling = e.target.nextSibling
        if(targetNextSibling.classList.contains('form-check-label'))
            targetNextSibling.classList.toggle(`strike-through`)
        if(targetNextSibling.classList.contains(`strike-through`))
            targetNextSibling.style.textDecoration='line-through ' 
        else
            targetNextSibling.style.textDecoration="none"
    }
})

//complete link
const completeLink = document.getElementById('completeLink')
completeLink.addEventListener('click',(e)=>{
    const ulList = document.getElementById('showItems')
    if(ulList)
        ulList.classList.toggle('hide');
    if(ulList.classList.contains('hide'))
        ulList.style.display = "none"
    else
        ulList.style.display='block'
})

//DROPDWON
const dropDown = document.getElementById('icon-dropDown')
dropDown.addEventListener('click',(e)=>{
    const ListChildren = List.children
    for(let item = 0 ; item<ListChildren.length;item++){
        const hasLabel = ListChildren[item].querySelector('label')
        const checkBox = ListChildren[item].querySelector('input[type=checkbox]')

       if(hasLabel){
            hasLabel.classList.toggle('strikeAll')
       }
       if(hasLabel.classList.contains('strikeAll')){
        hasLabel.style.textDecoration='line-through' 
         checkBox.checked = true
        }
        else{
        hasLabel.style.textDecoration='none' 
        checkBox.checked = false

    }
    }
    
})